#!/bin/bash

function hugo_build()
{
    echo "[+] Package $1 has been removed!"
    cd /data/
    echo "[+]  Rebuild mediacenter"
    time /usr/local/bin/hugo --templateMetrics  --debug --config /data/config.toml -d /tmp/hugo_builded

    echo "[+]  Clean up previous website version"
    find . -maxdepth 1 ! -name config.toml -a ! -name content -a ! -name themes -a ! -name olip_index.json -exec rm -rf '{}' \;

    echo "[+]  Deploy the new website version"
    cp -rf /tmp/hugo_builded/* /data/
    rm -rf /tmp/hugo_builded
}

[ -d /data/$1 ] && hugo_build
