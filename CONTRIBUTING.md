# Contributing to Media center

## Contributing with a new merge resquest

If you are about to make a Merge Request here is some things you should care as Test are not automatised yet :

* Search modal should render items based on search term
* Opensearch with olip should work as well
* Mediacenter should not rebuild at installation
* Media center should rebuild when a file change in `/data/content/`
* Mediacenter should be able to install new package
* `olip_index.json` should be generated at app's startup
* `olip_index.json` should be generated at each change in `/data/content`
* Metrics about build time should be logged out
* Sitemap must stay deactivate 

### Home
* Traduction should be available
* Breadcrumb should be present
* Image should be rendered
* Little media's icons should be rendered
* It should count the good amount of each media

### Package page
* JS language's filter should be here for multilingual package
* Image should be rendered for thumbnail
* There should be no link to others relative content
* There should be a title for each package item's
* A description of the package should be present at the top

### Item's page

* You must be able to download each specific resources (pdf / jpg / mp4 / mp3)


## Creating a release


```shell
export TAG=v1.0.0 && git tag -a -s -m "Release $TAG" "$TAG" && git push origin "$TAG"
```