# Media-center based on Hugo

[![pipeline status](https://gitlab.com/bibliosansfrontieres/olip/hugo-mediacenter/badges/master/pipeline.svg)](https://gitlab.com/bibliosansfrontieres/olip/hugo-mediacenter/commits/master)

## File tree

### |- api

Contains the code for the Express server API to server the opensearch end point for OLIP.
See [Implementing the search API](https://gitlab.com/bibliosansfrontieres/olip/olip-deploy/blob/master/app-developer.rst) for more informations about specifications

### |- docker

Script that will be use in the Docker image

-   `supervisord.conf` : will supervise the lunch and the run of `wait_for_change.sh` and `npm start` which is the Express server that server the opensearch API

-   `wait_for_change.sh` is looking in the folder `/data/content` for new folder made available by OLIP, for each new folder it will trigger `rebuild_hugo.sh` script

-   `rebuild_hugo.sh` clean up the previous build and build a new static site based on `/data/themes`, `/data/config.toml` and `/data/content` folder

-   `entrypoint.sh` clean up previous installation of Hugo. It will remove `/data/themes` and `/data/config.toml` before copying a new version of those files coming from the Docker image -> folder `/hugo-config/`

### |- hugo-config

Contains

-   `config.toml` Move at container start in \`/data/

-   `themes/my_themes`  Move at container start in `/data/`

### |- public

In other, contain `search` folder with `PageIndex.json` which is use for the search.

We should find a better solution to store the search index

### |- test

Contain the test for the API

## Basic content workflow

1.  End user trigger an package install
2.  OLIP unzip the package in `/data/content` (from the container), `/data/hugo.app/content` (from the host server)
3.  `wait_for_change.sh` see the new folder in `/data/content` and trigger `rebuild_hugo.sh`
4.  `rebuild_hugo.sh` build a static website in `/data` with the use of `/data/config.toml` and `/data/themes/`
5.  If the package is removed, `wait_for_change.sh` trigger an other time `rebuild_hugo.sh` that will remove all files in `/data/` except `config.toml`, `themes` and `content`

## Content format

Each item will be stored in a package. The media center will be constitued of packages.

Each package of items must use this format.

    apprendre-a-utiliser-des-patrons-fr
    ├── Comment_reproduire_un_patron.mp4
    ├── Comment_reproduire_un_patron.mp4.md
    ├── Comment_reproduire_un_patron.mp4.png
    ├── index.json
    ├── Reporter_un_patron_sur_tissu.mp4
    ├── Reporter_un_patron_sur_tissu.mp4.md
    └── Reporter_un_patron_sur_tissu.mp4.png

the command to zip the folder is the following

    $ cd apprendre-a-utiliser-des-patrons-fr
    $ zip -r apprendre-a-utiliser-des-patrons-fr.zip *

Markdown will follow something similar to :

    cat Comment_reproduire_un_patron.mp4.md
    +++
    date = "2019-02-12T13:18:17+04:00"
    title = "Comment réaliser un patron à partir d'un vêtement ?"
    draft = false
    image = "content/apprendre-a-utiliser-des-patrons-fr/Comment_reproduire_un_patron.mp4.png"
    showonlyimage = false
    weight = 2
    +++
    <video width="80%" height="auto" controls>
      <content src="../../content/apprendre-a-utiliser-des-patrons-fr/Comment_reproduire_un_patron.mp4" type="video/mp4">
    </video>
    
    Cette vidéo montre aux tailleurs comment réaliser un patron à partir d'un vêtement assemblé. Il prend l'exemple d'un t-shirt, mais peut être généralisé à tout type de vêtement.

Please note the **path** for

image
`image = "content/apprendre-a-utiliser-des-patrons-fr/Comment_reproduire_un_patron.mp4.png"`

content
`<content src="../../content/apprendre-a-utiliser-des-patrons-fr/Comment_reproduire_un_patron.mp4" type="video/mp4">`

## Continuous integration and delevery

Each `push` on `master` branch will trigger a new build of the Docker image.

The docker image is made available on [DockerHub](https://hub.docker.com/u/offlineinternet) with 3 arch : `amd64`, `arm32v7` and `i386`

### Re-build image

The Docker image can be re-build with

    docker build --build-arg ARCH=amd64 -f Dockerfile . -t offlineinternet/hugo-mediacenter-amd64:latest

And launch this way

    docker run -ti --name hugo --rm -p 8080:3000 -v /tmp/data/:/data offlineinternet/hugo-mediacenter-amd64:latest

### Update OLIP with the last image

The "descriptor" file holding the version number must be increase.

1.  Connect with SSH on dev server  : `olip-dev-mediacenter.bsf-intranet.org`
2.  edit `/data/descriptor/descriptor.json` and find the following section :

        {
        "applications":[
            {
            "bundle": "hugo.app",
            "name": "Mediacenter",
            "description": "Mediacenter for OLIP",
            "version": "0.1.2",
            "picture": "#generic.png",
            "containers": [
                {
                "image": "offlineinternet/hugo-mediacenter-amd64:latest",
                "name": "hugo",
                "expose": 3000
                }
            ],

    increase the field `version` by 0.0.1 and save the file.

3.  Call  <http://olip-dev-mediacenter.bsf-intranet.org:5002/applications/?visible=true&repository_update=true> to update the version of this file
4.  Go to <http://olip-dev-mediacenter.bsf-intranet.org/installed> and click on the **yellow** button. This will re-install the last version of the App automatically.
5.  Click on button **content** to manage local content
6.  Look at the result here: <http://olip-dev-mediacenter.bsf-intranet.org:10000/>

## Hugo development 

This method of developing is the closest way to stick to the production version

### Clone the repository 

```shell
git clone git@gitlab.com:bibliosansfrontieres/olip/hugo-mediacenter.git
cd hugo-mediacenter
```

### Download some content 

```shell
mkdir data/content/
cd data/content/
wget http://packages.ideascube.org/hugo/fight-against-discriminations-and-live-together-en.zip
mkdir fight-against-discriminations-and-live-together-en
cd fight-against-discriminations-and-live-together-en
unzip ../fight-against-discriminations-and-live-together-en.zip

```

### Launch the container

```shell
cd hugo-mediacenter
docker run -it --entrypoint "/docker/entrypoint-dev.sh" --rm -p 80:3000 -v $(pwd)/hugo-config/config.toml:/data/config.toml -v $(pwd)/hugo-config/themes/:/data/themes -v $(pwd)/data/content:/data/content/ --name hugo offlineinternet/hugo-mediacenter-amd64 sh
```

At this point Hugo (in the container) build the website from the source based on 

* the themes `hugo-config/themes/`
* the content `data/content`
* using the config file `config.toml`

The whole thing is stored in `/data/` and served by `Node JS Express webserver`

Once launch the container will show you 

```
[nodemon] 1.19.4
[nodemon] to restart at any time, enter `rs`
[nodemon] watching dir(s): *.*
[nodemon] watching extensions: js,mjs,json
[nodemon] starting `node app.js`
Media center appplication RESTful API server started on: 3000
```

Hit Enter and you'll get the prompt back

After modifying the sources launch Hugo again to build the website

```
cd /data/ && /usr/local/bin/hugo --config /data/config.toml -d /data/
```

Result are available on : http://localhost

## Ressources

### Search

-   <https://fr.jeffprod.com/blog/2018/un-moteur-de-recherche-interne-pour-votre-site-hugo/>
-   <https://codepen.io/JMChristensen/pen/PpweZP>
-   <https://lunrjs.com/>
-   <https://gitlab.com/kaushalmodi/hugo-search-fuse-js>
